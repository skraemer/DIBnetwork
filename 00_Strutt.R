# Kinetic modelling of bacterial growth in DIB network with antibiotic
# Script by S. Krämer, R. Strutt et al.
# first version 19.8.2023; current version 12.8.2024

rm(list=ls())
gc()
# graphics.off()
script.dir <- dirname(rstudioapi::getActiveDocumentContext()$path)
setwd(script.dir)

CalcStart <- Sys.time()
print(CalcStart)
## Set directory to current directory

library("pracma")
library("stats")
library("deSolve")
library("HelpersMG")
source("01_Strutt_Functions.R")

ResultsFolder0 <- "Results_240811"

Folders <- dir(getwd())
NumResultsFolders <- sapply(script.dir,function(dir)
{length(list.files(dir,pattern=substr(ResultsFolder0, start = 1, stop = nchar(ResultsFolder0))))})
ResultsFolder <- paste(ResultsFolder0,"_",sprintf("%03.0f",NumResultsFolders+1),sep="")
print(ResultsFolder)
dir.create(ResultsFolder,showWarnings = FALSE)
ResultsFileName <- paste(ResultsFolder,".csv",sep="")
ResultsFile <- file.path(ResultsFolder,ResultsFileName,fsep=.Platform$file.sep)
FigPath <- paste(ResultsFolder,"/Figs",sep="")
dir.create(FigPath,showWarnings = FALSE)
file.copy("00_Strutt.R",ResultsFolder,overwrite = TRUE)
file.copy("01_Strutt_Functions.R",ResultsFolder,overwrite = TRUE)
closeAllConnections()
PlotNum <- 0

NamesDataFiles <- dir("Data")
Data.list <- list()
for (nF in 1:length(NamesDataFiles)){
  Data.list[[nF]] <- read.csv(file.path("Data",NamesDataFiles[nF],fsep=.Platform$file.sep))
}
names(Data.list) <- NamesDataFiles
T_DIBform_min <- c(NA,NA,60,150)+0

# Plot data ####################################################################
PlotNum <- PlotNum+1
PlotName <- file.path(FigPath,
                      paste('Fig','_',sprintf("%04.0f", PlotNum),'.jpg',
                            sep=""),fsep=.Platform$file.sep)
jpeg(file=PlotName, width=24,height=8,units='cm', res=300)
par(mfrow=c(1,3))
Datai1 <- Data.list[[1]]
Datai2 <- Data.list[[2]]
plot(Datai1$Time_min/60,Datai1$Signal_Mean,
     xlim=c(0,max(Datai1$Time_min/60,Datai2$Time_min/60)),
     ylim=c(min(c(Datai1$Signal_Mean-Datai1$Signal_SD),(Datai2$Signal_Mean-Datai2$Signal_SD)),
            max(c(Datai1$Signal_Mean+Datai1$Signal_SD),(Datai2$Signal_Mean+Datai2$Signal_SD))),
     xlab="Time (h)",ylab="Signal",col='blue')
polygon(c((Datai1$Time_min/60),(Datai1$Time_min/60)[length(Datai1$Time_min):1]),
        c((Datai1$Signal_Mean-Datai1$Signal_SD),(Datai1$Signal_Mean+Datai1$Signal_SD)[length(Datai1$Time_min):1]),
        border=NA,col=adjustcolor( "grey", alpha.f = 0.2),density=NA)
points(Datai2$Time_min/60,Datai2$Signal_Mean,col='red')
polygon(c((Datai2$Time_min/60),(Datai2$Time_min/60)[length(Datai2$Time_min):1]),
        c((Datai2$Signal_Mean-Datai2$Signal_SD),(Datai2$Signal_Mean+Datai2$Signal_SD)[length(Datai2$Time_min):1]),
        border=NA,col=adjustcolor( "grey", alpha.f = 0.2),density=NA)
for (nF in 3:4){
  Datai <- Data.list[[nF]]
  iCol <- match(c("Time_min","Droplet_Doxy","Droplet_1","Droplet_2","Droplet_3","Droplet_4"),colnames(Datai))
  Datai <- Datai[,iCol]
  Data.list[[nF]] <- Datai
  plot(Datai$Time_min/60,rep(NA,length(Datai$Time_min/60)),
       ylim=range(Datai[,(2:dim(Datai)[2])]),
       xlab="Time (h)", ylab="Signal",
       main=NamesDataFiles[nF])
  for (nD in 2:dim(Datai)[2]){
    points(Datai$Time_min/60,Datai[,nD],
           col=nD)
  }
  abline(v=(T_DIBform_min[nF]/60),lty=3)
  legend("topleft",legend=colnames(Datai)[2:dim(Datai)[2]],
         pch=1,col=2:dim(Datai)[2],bty='n')
}
dev.off()

# Growth curves exponentials ##################################################
par0.list <- list()
par0.list[[1]] <- c(500,2200,0.01,3,
                    3000,0.0008,2.2)
par0.list[[2]] <- par0.list[[1]] * c(1,0.2,1,1,1,1,1)

fit.list <- list()
par(mfrow=c(2,2))
for (nF in 1:2){
  Datai <- Data.list[[nF]]
  Time_min <- Datai$Time_min
  Signal <- Datai$Signal_Mean
  SimSignal <- SimGrowth(par0.list[[nF]],Time_min)
  if (1 == 0){
    plot(Time_min/60,Signal,ylim=range(c(Signal,SimSignal)),
         xlab="Time (h)",ylab="Signal",main="Start values")
    lines(Time_min/60,SimSignal)
  }
  
  fit.list[[nF]] <- optim(par=par0.list[[nF]],fn=SSRGrowth,gr=NULL,Time_min=Time_min,Signal=Signal,
                          control=list(maxit=10000), hessian=TRUE)
  
  SimSignal <- SimGrowth(fit.list[[nF]]$par,Time_min)
  if (1 == 0){
    plot(Time_min/60,Signal,ylim=range(c(Signal,SimSignal)),
         xlab="Time (h)",ylab="Signal",main="Fit")
    lines(Time_min/60,SimSignal)
  }
}

# Doxy diffusion ###############################################################
lkDiff <- log(2.065e-4 * 3600) # 1/h
lkLost <- log(exp(lkDiff) * 0.030)
y0 <- c(1,0,0,0,0,0)
parms0 <- c(lkDiff,lkLost)
## Linear network
Datai <- Data.list[[4]]
Time.min <- Datai$Time_min - T_DIBform_min[4]
Time.min <- unique(Time.min[Time.min>=0])
DoxySimLin <- ode(y=y0,times=Time.min/60,func=funDoxyLin,parms=parms0)
DoxySimLin.df <- as.data.frame(DoxySimLin)
colnames(DoxySimLin.df) <- c("Time.h","Droplet_Doxy","Droplet_1","Droplet_2",
                             "Droplet_3","Droplet_4","Doxy_Lost")

Doxy.list <- list()
Doxy.list[[1]] <- NA
Doxy.list[[2]] <- DoxySimLin.df

PlotNum <- PlotNum+1
PlotName <- file.path(FigPath,
                      paste('Fig','_',sprintf("%04.0f", PlotNum),'.jpg',
                            sep=""),fsep=.Platform$file.sep)
jpeg(file=PlotName, width=12,height=12,units='cm', res=300)
par(mfrow=c(1,1))
plot(DoxySimLin.df$Time.h,DoxySimLin.df$Droplet_Doxy,ty='l',
     ylim=c(0,max(DoxySimLin.df$Droplet_Doxy)),
     xlab="Time (h)",ylab="Relative doxycycline",
     col=2,main="Doxy in linear system")
for (nD in 3:dim(DoxySimLin.df)[2]){
  lines(DoxySimLin.df$Time.h,DoxySimLin.df[,nD],col=nD)
}
legend("topright",legend=colnames(DoxySimLin.df)[2:length(DoxySimLin.df)],
       lty=1,col=2:(dim(DoxySimLin.df)[2]),bty='n')
dev.off()

PlotNum <- PlotNum+1
PlotName <- file.path(FigPath,
                      paste('Fig','_',sprintf("%04.0f", PlotNum),'.jpg',
                            sep=""),fsep=.Platform$file.sep)
jpeg(file=PlotName, width=12,height=12,units='cm', res=300)
par(mfrow=c(1,1))
for (nF in 4:4){
  Datai <- Data.list[[nF]]
  plot(Datai$Time_min/60,rep(NA,length(Datai$Time_min/60)),
       ylim=range(Datai[,(2:dim(Datai)[2])]),
       xlab="Time (h)", ylab="Signal",
       main=NamesDataFiles[nF])
  for (nD in 2:dim(Datai)[2]){
    points(Datai$Time_min/60,Datai[,nD],
           col=nD)
  }
  abline(v=(T_DIBform_min[nF]/60),lty=3)
  
  for (nD in 2:(dim(DoxySimLin.df)[2]-1)){
    lines((Doxy.list[[nF-2]]$Time.h+T_DIBform_min[nF]/60),(Doxy.list[[nF-2]][,nD]*(max(Datai[,-1])-500)+500),col=nD)
  }
  legend("topleft",legend=colnames(Datai)[2:dim(Datai)[2]],
         pch=1,col=2:dim(Datai)[2],bty='n')
}
dev.off()

# Bacterial growth ODE #############################################################
par(mfrow=c(4,5),mar=c(2,2,2,1))
fitODE.list <- list()
fitODE.list[[1]] <- NA
fitODE.list[[2]] <- NA

NamesY <- c("D0","D1","D2","D3","D4","DL","G0","G1","G2","G3","G4","G0i","G1i","G2i","G3i","G4i")
funGrowth <- funGrowthLin
parms0.fix <- c(lkDiff=lkDiff,lkLost=lkLost,ltDIB=log(T_DIBform_min[[nModel]]/60))
parms0.fit <- c(ltLag=log(2.5),lklag=log(0.1),lkexp=log(1.6),lkinh2=log(30),lkinh1=log(0.07))
par(mfrow=c(4,5),mar=c(2,2,2,1))
Datai <- Data.list[[nModel]]
Time.min <- Datai$Time_min
Time.h <- Time.min/60
Datai[,2:dim(Datai)[2]] <- apply(Datai[,2:dim(Datai)[2]],2,function(x){x/x[1]})
y0 <- c(1,rep(0,5),unname(unlist(Datai[1,2:dim(Datai)[2]])),rep(0,5))
names(y0) <- NamesY
  
sfGFPode <- ode(y=y0,times=Time.h,func=funGrowth,parms=c(parms0.fix,parms0.fit))
sfGFPode.df <- as.data.frame(sfGFPode)
sfGFPode.colnames <- c("Time.h","D0","D1","D2","D3","D4","DL",
                         "sfGFP0","sfGFP1","sfGFP2","sfGFP3","sfGFP4",
                         "sfGFPi0","sfGFPi1","sfGFPi2","sfGFPi3","sfGFPi4")
colnames(sfGFPode.df) <- sfGFPode.colnames
sfGFPode.df <- cbind(sfGFPode.df,sfGFPode.df[which(colnames(sfGFPode.df)=="sfGFP0"):
                                                which(colnames(sfGFPode.df)=="sfGFP4")]+
                         sfGFPode.df[which(colnames(sfGFPode.df)=="sfGFPi0"):
                                      which(colnames(sfGFPode.df)=="sfGFPi4")])
colnames(sfGFPode.df) <- c(sfGFPode.colnames,"sfGFPtotal0","sfGFPtotal1","sfGFPtotal2","sfGFPtotal3","sfGFPtotal4")
## Figure simultion with parms0 ##################################################
jpg <- 0
if (1==0){
  PlotNum <- PlotNum+1
  PlotName <- file.path(FigPath,
                          paste('Fig','_',sprintf("%04.0f", PlotNum),'.jpg',
                                sep=""),fsep=.Platform$file.sep)
  jpeg(file=PlotName, width=16,height=16,units='cm', res=300)
  jpg <- 1
  par(mfrow=c(1,1),mar=c(5, 4, 4, 2) + 0.1)
}
plot((sfGFPode.df$Time.h),rep(NA,dim(sfGFPode.df)[1]),
      ylim=range(sfGFPode.df[,which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]],Datai[,2:dim(Datai)[2]],na.rm=TRUE),
      xlab="Time (h)", ylab="sfGFP signal",main=NamesDataFiles[nModel],
      abline(v=(T_DIBform_min[nModel]/60),lty=3,col='grey'))
for (nD in 2:dim(Datai)[2]){
  points((Datai$Time_min/60),Datai[,nD],col=nD)
}
for (nD in which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]){
  lines(sfGFPode.df$Time.h,sfGFPode.df[,nD],col=nD-16)
}
SSRsim <- sum(((unname(unlist(Datai[,3:dim(Datai)[2]]))) -
                   (unname(unlist(sfGFPode.df[,which(colnames(sfGFPode.df)=="sfGFPtotal1"):dim(sfGFPode.df)[2]]))))^2)
legend("topleft",legend=c(paste("SSR = ",round(SSRsim,3),sep=""),
                            signif(exp(c(parms0.fit)),3)),bty='n')
if (jpg==1){
  dev.off()
}
###############################################################################
# Vary the paramters
SimResults <- as.data.frame(t(c(parms0.fix,parms0.fit)))
if (1==0){
  for (n1 in 0){
    for (n2 in 0){
      for (n3 in seq(-4,4,0.4)){
        for (n4 in seq(-4,4,0.4)){
          for (n5 in seq(-2,1,0.5)){
            parms0i.fit <- parms0.fit+c(n1,n2,n3,n4,n5)
            sfGFPode <- ode(y=y0,times=Time.h,func=funGrowth,parms=c(parms0.fix,parms0i.fit))
            print(c(n1,n2,n3,n4,n5))
            sfGFPode.df <- as.data.frame(sfGFPode)
            colnames(sfGFPode.df) <- sfGFPode.colnames
            sfGFPode.df <- cbind(sfGFPode.df,sfGFPode.df[which(colnames(sfGFPode.df)=="sfGFP0"):
                                                             which(colnames(sfGFPode.df)=="sfGFP4")]+
                                     sfGFPode.df[which(colnames(sfGFPode.df)=="sfGFPi0"):
                                                   which(colnames(sfGFPode.df)=="sfGFPi4")])
            colnames(sfGFPode.df) <- c(sfGFPode.colnames,"sfGFPtotal0","sfGFPtotal1","sfGFPtotal2","sfGFPtotal3","sfGFPtotal4")
            SSRsimi <- sum(((unname(unlist(Datai[,3:dim(Datai)[2]]))) -
                                (unname(unlist(sfGFPode.df[,which(colnames(sfGFPode.df)=="sfGFPtotal1"):dim(sfGFPode.df)[2]]))))^2)
            SSRsim <- c(SSRsim,SSRsimi)
            SimResultsi <- as.data.frame(t(c(parms0.fix,parms0i.fit)))
            SimResults <- rbind(SimResults,SimResultsi)
              
            if (!is.na(SSRsimi) & SSRsimi <= min(SSRsim,na.rm=TRUE)){
              jpg <- 0
              if (1==1){
                PlotNum <- PlotNum+1
                PlotName <- file.path(FigPath,
                                        paste('Fig','_',sprintf("%04.0f", PlotNum),'.jpg',
                                              sep=""),fsep=.Platform$file.sep)
                jpeg(file=PlotName, width=16,height=16,units='cm', res=300)
                par(mfrow=c(1,1))
                jpg <- 1
              }
                
              plot(sfGFPode.df$Time.h,rep(NA,dim(sfGFPode.df)[1]),
                     ylim=range(sfGFPode.df[,which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]],
                                Datai[,2:dim(Datai)[2]],na.rm=TRUE),
                     xlab="Time (h)", ylab="sfGFP signal",main=NamesDataFiles[nModel],
                     abline(v=(T_DIBform_min[nModel]/60),lty=3,col='grey'))
              for (nD in 2:dim(Datai)[2]){
                points((Datai$Time_min/60),Datai[,nD],col=nD)
              }
              for (nD in which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]){
                lines(sfGFPode.df$Time.h,sfGFPode.df[,nD],col=nD-16)
              }
              legend("topleft",legend=c(paste("SSR = ",round(SSRsimi,3),sep=""),
                                          signif(exp(c(parms0i.fit)),3)),bty='n')
              if (jpg==1){
                dev.off()
              }
            }
          }
        }
      }
    }
  }
}
  
###############################################################################
## Fit growth ODE ################################################################
SSRGrowthODE <- function(Time.h,y,parms.fit,parms.fix){
  parms <- c(parms.fix,parms.fit)
  ODEsim <- ode(y=y,times=Time.h,func=funGrowth,parms=c(parms.fix,parms.fit))
  ODEsim.df <- as.data.frame(ODEsim)
  colnames(ODEsim.df) <- sfGFPode.colnames
  ODEsim.df <- cbind(ODEsim.df,ODEsim.df[which(colnames(ODEsim.df)=="sfGFP0"):
                                                   which(colnames(ODEsim.df)=="sfGFP4")]+
                         ODEsim.df[which(colnames(ODEsim.df)=="sfGFPi0"):
                                         which(colnames(ODEsim.df)=="sfGFPi4")])
    
  colnames(ODEsim.df) <- c(sfGFPode.colnames,"sfGFPtotal0","sfGFPtotal1","sfGFPtotal2","sfGFPtotal3","sfGFPtotal4")
  SSR <- sum(((unname(unlist(Datai[,3:dim(Datai)[2]]))) - # changed from [,2:...] to [,3:...], 11.8.2024
                  unname(unlist(ODEsim.df[,which(colnames(ODEsim.df)=="sfGFPtotal1"):dim(ODEsim.df)[2]])))^2,na.rm=TRUE) # changed from "sfGFPtotal0" to "sfGFPtotal1", 11.8.2024
  return(SSR)
}
Tstart <- Sys.time()
print(paste("Fit started at ",Tstart,", Model: ",nModel,sep=""))
fitODE.list[[nModel]] <- optim(par=parms0.fit,fn=SSRGrowthODE,gr=NULL,
                                 method="L-BFGS-B",
                                 y=y0,Time.h=Time.h,parms.fix=parms0.fix,
                                 control=list(maxit=200000, tmax=10, trace=1),
                                 hessian=TRUE)
  
## SE of the parameters
fitODE.list[[nModel]]$SE <- tryCatch({SEfromHessian(fitODE.list[[nModel]]$hessian, hessian=FALSE, silent=FALSE)},
  error = function(cond) {
  message("SE failed")
  rep(NA,length(parms0.fit))
},
warning = function(cond) {
  message("SE warning")
  SEfromHessian(fitODE.list[[nModel]]$hessian, hessian=FALSE, silent=FALSE)
},
finally = {
  NULL
})
  
Tcalc <- Sys.time() - Tstart
print(Tcalc)
  
sfGFPode <- ode(y=y0,times=seq(0,8,0.01),func=funGrowth,parms=c(parms0.fix,fitODE.list[[nModel]]$par))
sfGFPode.df <- as.data.frame(sfGFPode)
colnames(sfGFPode.df) <- sfGFPode.colnames
sfGFPode.df <- cbind(sfGFPode.df,sfGFPode.df[which(colnames(sfGFPode.df)=="sfGFP0"):
                                                 which(colnames(sfGFPode.df)=="sfGFP4")]+
                         sfGFPode.df[which(colnames(sfGFPode.df)=="sfGFPi0"):
                                       which(colnames(sfGFPode.df)=="sfGFPi4")])
colnames(sfGFPode.df) <- c(sfGFPode.colnames,"sfGFPtotal0","sfGFPtotal1","sfGFPtotal2","sfGFPtotal3","sfGFPtotal4")
  
## Figure fit #################################################################
jpg <- 0
if (1==1){
  PlotNum <- PlotNum+1
  PlotName <- file.path(FigPath,
                          paste('Fig','_',sprintf("%04.0f", PlotNum),'.jpg',
                                sep=""),fsep=.Platform$file.sep)
  jpeg(file=PlotName, width=16,height=16,units='cm', res=300)
  jpg <- 1
}
par(mfrow=c(1,1))
plot(sfGFPode.df$Time.h,rep(NA,dim(sfGFPode.df)[1]),
       ylim=range(0.5,sfGFPode.df[,which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]],
                  Datai[,2:dim(Datai)[2]]),
       xlab="Time (h)", ylab="sfGFP signal",main=NamesDataFiles[nModel],
       abline(v=(T_DIBform_min[nModel]/60),lty=3,col='grey'))
for (nD in 2:dim(Datai)[2]){
  points(Datai$Time_min/60,Datai[,nD],col=nD)
}
for (nD in which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]){
  lines(sfGFPode.df$Time.h,sfGFPode.df[,nD],col=nD-16,lty=3)
}
legend("topleft",legend=c(colnames(sfGFPode.df)[which(colnames(sfGFPode.df)=="sfGFPtotal0"):dim(sfGFPode.df)[2]],
                            bquote(italic(t)[lag]~.(signif(exp(fitODE.list[[nModel]]$par[1]),3))~h),
                            bquote(italic(k)[lag]~.(signif(exp(fitODE.list[[nModel]]$par[2]),3))~h^{-1}),
                            bquote(italic(k)[growth]~.(signif(exp(fitODE.list[[nModel]]$par[3]),3))~h^{-1}),
                            bquote(italic(k)[inh]~.(signif(exp(fitODE.list[[nModel]]$par[4]),3))~h^{-1}),
                            bquote(italic(f)[i0]~.(signif(exp(fitODE.list[[nModel]]$par[5]),3))),
                            paste("SSR ",round(fitODE.list[[nModel]]$value,3))),
         pch=c(rep(1,5),rep(NA,length(fitODE.list[[nModel]]$par)+1)),
         col=c(2:6,rep(NA,(length(fitODE.list[[nModel]]$par)+1))),bty='n')
legend("bottomleft",legend=bquote(italic(f)[i0]=="("*italic(k)[inh]~"intrinsic)"~"/"~"("*italic(k)[inh]~"at max [doxy])"),
         cex=0.75,bty='n')
if (jpg==1){
  dev.off()
  file.show(PlotName)
}
print(sprintf("%04.4f", exp(parms0.fit)))
print(sprintf("%04.4f", exp(fitODE.list[[nModel]]$par)))
  
FitResults <- data.frame(t(c(parms0.fix,fitODE.list[[nModel]]$par)))
FitResults <- rbind(FitResults,c(rep(NA,length(parms0.fix)),fitODE.list[[nModel]]$SE))
FitResults <- rbind(FitResults,c(rep(0,length(parms0.fix)),rep(1,length(parms0.fit))))
rownames(FitResults) <- c("Value","SE","Fix0_Fit1")
  
write.csv(SimResults,file.path(ResultsFolder,
                                 paste("SimResults",nModel,".csv",sep=""),fsep=.Platform$file.sep))
write.csv(FitResults,file.path(ResultsFolder,
                                 paste("FitResults",nModel,".csv",sep=""),fsep=.Platform$file.sep))
write.csv(sfGFPode.df,file.path(ResultsFolder,paste("Simulated",nModel,".csv",sep=""),fsep=.Platform$file.sep))
save.image(file.path(ResultsFolder,"Environment.RData",fsep=.Platform$file.sep))
