# Dib Strutt Et Al

## Description
R scripts for the modelling of doxycycline concentration in droplets separated by DIBs and of bacterial growths in the same droplets. 00_Strutt.R contains the main script. 01_Strutt_Functions.R contains the functions for simulation and fitting, including the ODE system for simulating the bacterial growths.

## Usage
The R script 00_Strutt.R reads csv data files, in the uploaded script version located in a folder with the name "Data". Data files are not provided here.

## Support
e-mail stefanie.kraemer@pharma.ethz.ch; robert.strutt@bsse.ethz.ch; petra.dittrich@bsse.ethz.ch

## Authors and acknowledgment
This work is published as "Biohybrid artificial cells link treatment efficacy to membrane depth in bacterial infections" by Robert Strutt, Petra Jusková, Simon F. Berlanda, Stefanie D. Krämer and Petra S. Dittrich in xxxx [Journal to be updated]

## License
CC BY https://creativecommons.org/share-your-work/cclicenses/

## Project status
Manuscript submitted.
